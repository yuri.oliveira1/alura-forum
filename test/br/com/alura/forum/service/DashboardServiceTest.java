package br.com.alura.forum.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import br.com.alura.forum.dao.CategoriaDao;
import br.com.alura.forum.dao.TopicoDao;
import br.com.alura.forum.model.Categoria;
import br.com.alura.forum.model.vo.DashboardItem;

@ExtendWith(MockitoExtension.class)
class DashboardServiceTest {

	@InjectMocks
	private DashboardService dashboardService;

	@Mock
	private CategoriaDao categoriaDao;

	@Mock
	private TopicoDao topicoDao;

	private List<Categoria> categorias;

	@BeforeEach
	public void before() {
		Categoria backend = new Categoria("Back-end");
		Categoria frontend = new Categoria("Front-End");
		Categoria mobile = new Categoria("Mobile");

		this.categorias = Arrays.asList(backend, frontend, mobile);
	}

	@Test
	public void quandoNaoHaTopicosAbertosContadoresDevemEstarZerados() {

		// Configura comportamento do Mock
		Mockito.when(categoriaDao.buscarTodasAsCategoriasPrincipais()).thenReturn(categorias);

		categorias.forEach(categoria -> {
			Mockito.when(topicoDao.countPorCategoria(categoria)).thenReturn(0L);
			Mockito.when(topicoDao.countPorCategoriaEAbertosNaUltimaSemana(categoria)).thenReturn(0L);
			Mockito.when(topicoDao.countPorCategoriaENaoRespondidos(categoria)).thenReturn(0L);
		});
		
		final List<DashboardItem> dashboard = dashboardService.buscarDadosDoDashboardDeTopicos();
		
		assertEquals(3, dashboard.size());
		dashboard.forEach(dashboardItem -> {
			assertEquals(0, dashboardItem.getQtdTopicos());
			assertEquals(0, dashboardItem.getQtdTopicosDaUltimaSemana());
			assertEquals(0, dashboardItem.getQtdTopicosNaoRespondidos());
		}); 
		
	}

}

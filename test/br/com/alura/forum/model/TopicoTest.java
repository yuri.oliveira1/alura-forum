package br.com.alura.forum.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TopicoTest {
	
	private Usuario usuario;
	private Curso htmlBasico;

	@BeforeEach
	public void before() {
		this.usuario = new Usuario("Ana", "ana@email.com", "123456");
		Categoria frontend = new Categoria("Front-End");
		Categoria html = new Categoria("HTML", frontend);
		this.htmlBasico = new Curso("HTML B�sico", html);
	}

	@Test
	void quandoFechaTopicoAtualizaStatusParaFechado() {
		Topico topico = new Topico("D�vida HTML", "Qual tag utilizar?", usuario, htmlBasico);
		topico.fechar();
		
		assertEquals(StatusTopico.FECHADO, topico.getStatus());
	}

}
